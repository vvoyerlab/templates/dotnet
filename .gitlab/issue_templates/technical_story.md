## Description and Objective

_**Tip**: Explain your motivations and objectives so that the responsible person can consider any alternative solutions if necessary._

## Tasks and Activities

- [ ] Task 1
- [ ] Task 2
- ...

## Acceptance Criteria

- [ ] The system has...
- [ ] We have a tool for...
- [ ] Data is successfully migrated...

/label ~enhancement

> Source: [playbook.sparkfabrik.com - Gitlab issue templates](https://playbook.sparkfabrik.com/tools-and-policies/gitlab-issue-templates)
