## Problem Description

_Provide a concise description of the bug._

## Steps to Reproduce

_List the steps to reproduce the problem:_

1. Visit '...'
2. Click on '...'
3. Scroll down to '...'
4. ...

### Expected Behavior

_A clear and concise description of what you expect to happen._

### Observed Behavior

_A clear and concise description of what actually happens instead._

## Screenshots

_If relevant, copy-paste screenshots or photos of the issue here._

/label ~bug

> Source: [playbook.sparkfabrik.com - Gitlab issue templates](https://playbook.sparkfabrik.com/tools-and-policies/gitlab-issue-templates)
