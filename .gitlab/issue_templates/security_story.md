# Security story

## Description of Need

**As a** <role>  
**I want** <feature>  
**So that** <objective>

## Security scenario

### Scenario title

**Given** <context>  
**When** <action>  
**Then** <result>

## Notes

-
-
-

/label ~critical ~enhancement

> Source: [playbook.sparkfabrik.com - Gitlab issue templates](https://playbook.sparkfabrik.com/tools-and-policies/gitlab-issue-templates)
