![vscode](https://img.shields.io/badge/VSCode-0078D4?style=for-the-badge&logo=visual%20studio%20code&logoColor=white) ![dotnet](https://img.shields.io/badge/C%23-512BD4?style=for-the-badge&logo=csharp&logoColor=white) ![taskfile](https://img.shields.io/badge/task-29BEB0?style=for-the-badge&logo=task&logoColor=white) ![gitlab](https://img.shields.io/badge/gitlab-FC6D26?style=for-the-badge&logo=gitlab&logoColor=white)

# Dotnet VSCode Devconatainer Project Template

This project is a template for dotnet developpement into dev container.

## What's included ?

- [VScode Extension kit](#vs-code-extensions)
- Dotnet docker environment
- [Taskfile](#tasks-available)
- Gitlab Issue templates
- Gitlab Merge Request template for review

## Structure

```
project/
├── .config/
|    ├── tasks/
|    |    ├── decision/
|    |    |    └── Taskfile.yml
|    |    ├── dotnet/
|    |    |    └── Taskfile.yml
|    |    └── release/
|    |         └── Taskfile.yml
|    └── templates
|         ├── decision.md
|         └── release.md
├── .gitlab/
|    ├── issue_templates/
|    |    ├── bug_story.md
|    |    ├── security_story.md
|    |    ├── support.md
|    |    ├── technical_story.md
|    |    └── user_story.md
|    └── merge_request_templates/
|         └── review.md
├── .vscode/
|    └── settings.json
├── documentations/
|    ├── decisions/
|    |    └── {YYYY-MM-DD}-{subject}.md
|    └── releases/
|         └── v{version}.md
├── sources/
|    └── DotnetProject/
├── tests/
|    └── DotnetTestProject/
|
├── .gitignore
├── project.sln
├── README.md
└── Taskfile.yml
```

## Tasks available

### Decision

* **decision:create** : Create a new decision file. usage task decision:create -- subject

### Dotnet

* **dotnet:clean** : Clean the solution
* **dotnet:create-api** : Create a new api project. usage dotnet:create-api -- project-name
* **dotnet:create-classlib** : Create a new classlib project. usage dotnet:create-classlib -- project-name
* **dotnet:create-console** : Create a new console project. usage dotnet:create-console -- project-name
* **dotnet:create-test** : Create a new test project. usage dotnet:create-test -- project-name
* **dotnet:create-worker** : Create a new worker project. usage dotnet:create-worker -- project-name
* **dotnet:remove-api** : Remove an api project. usage dotnet:remove-api -- project-name
* **dotnet:remove-classlib** : Remove a classlib project. usage dotnet:remove-classlib -- project-name
* **dotnet:remove-console** : Remove a console project. usage dotnet:remove-console -- project-name
* **dotnet:remove-test** : Remove a test project. usage dotnet:remove-test -- project-name
* **dotnet:remove-worker** : Remove a worker project. usage dotnet:remove-worker -- project-name
* **dotnet:run-all-tests** : Run all test projects
* **dotnet:run-test** : Run specific test project. usage dotnet:run-test -- project-name

### Release

* **release:create** : Create a new release file. usage task release:create -- version

## VS Code Extensions

* [Code spell checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)
* [C# Dev Kit](https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.csdevkit)
* [Task](https://marketplace.visualstudio.com/items?itemName=task.vscode-task)
* [Yaml](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-yaml)
* [Markdown Preview Enhanced](https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced)
* [Draw.io Integration](https://marketplace.visualstudio.com/items?itemName=hediet.vscode-drawio)
* [GitHub Copilot](https://marketplace.visualstudio.com/items?itemName=GitHub.copilot)